package com.pachinko.poker.star.rules.app.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pachinko.poker.star.rules.app.R
import com.pachinko.poker.star.rules.app.models.Rules


class RulesAdapter(private val combo: ArrayList<Rules>) :
    RecyclerView.Adapter<RulesAdapter.ViewHolder>() {

    var pos = 0

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val rulesImage: ImageView = itemView.findViewById(R.id.image)
        val rulesTitle: TextView = itemView.findViewById(R.id.title)
        val rulesDescription: TextView = itemView.findViewById(R.id.desc)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.rules_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val rulesList = combo[position]
        pos = holder.adapterPosition
        holder.rulesDescription.text = rulesList.rulesDesc
        rulesList.rulesPicture?.let { holder.rulesImage.setImageResource(it) }
        holder.rulesTitle.text = rulesList.rulesTitle

    }

    override fun getItemCount(): Int = combo.size
}