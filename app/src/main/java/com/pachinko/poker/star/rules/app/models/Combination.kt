package com.pachinko.poker.star.rules.app.models

data class Combination(
    val comboTitle: String?,
    val comboPicture: Int?,
    val comboDesc: String
)
