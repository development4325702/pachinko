package com.pachinko.poker.star.rules.app.models

data class Rules(
    val rulesTitle: String?,
    val rulesDesc: String,
    val rulesPicture: Int?

)
