package com.pachinko.poker.star.rules.app.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pachinko.poker.star.rules.app.R
import com.pachinko.poker.star.rules.app.models.Combination


class CombinationsAdapter(private val combo: ArrayList<Combination>) :
    RecyclerView.Adapter<CombinationsAdapter.ViewHolder>() {

    var pos = 0


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val comboImage: ImageView = itemView.findViewById(R.id.image)
        val comboTitle: TextView = itemView.findViewById(R.id.title)
        val comboDescription: TextView = itemView.findViewById(R.id.desc)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.combo_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val rulesList = combo[position]
        holder.comboDescription.text = rulesList.comboDesc
        rulesList.comboPicture?.let { holder.comboImage.setImageResource(it) }
        holder.comboTitle.text = rulesList.comboTitle
        pos = holder.adapterPosition
    }

    override fun getItemCount(): Int =
        combo.size

}