package com.pachinko.poker.star.rules.app.screens

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pachinko.poker.star.rules.app.R
import com.pachinko.poker.star.rules.app.adapters.CombinationsAdapter
import com.pachinko.poker.star.rules.app.databinding.ActivityCombinationsBinding
import com.pachinko.poker.star.rules.app.models.Combination

class CombinationsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCombinationsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCombinationsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setFullScreen()

        val comboData = ArrayList<Combination>()
        comboData.add(
            Combination(
                "A couple",
                R.drawable.combo_1,
                "It is a combination of two cards of the same denomination and three other cards of different denominations. One of the basics in poker, it occurs quite often. For example, you have two shafts, and on the table there are 3, 7, 10, King. In this case, you have a pair of vallets.\n" +
                        "The probability of getting a pair depends on many factors, such as the number of players at the table, the number of decks that are used, and others. The chances of success are about 5 to 11, which makes the pair a fairly common combination."
            )
        )

        comboData.add(
            Combination(
                "Set",
                R.drawable.combo_2,
                "It consists of three cards of the same denomination and two additional ones. Stronger than a pair and has a higher cost in hand. For example, you have three ladies, and on the table there are 4, Lady, 9, 2, 8. In this case, a set of ladies is made.\n" +
                        "The probability of getting a set is quite low. It is approximately 1 to 20. This makes the combination valuable, so players strive to collect it to increase the chances of winning."
            )
        )

        comboData.add(
            Combination(
                "Royal flash",
                R.drawable.combo_3,
                "It is by far the most powerful and cherished combination in poker. This is a set of five cards of the same suit, arranged sequentially from ten to an ace. For example, 10, jack, queen, king and ace of spades are the real embodiment of the royal flush.\n" +
                        "The probability of getting a royal flush is extremely low and is only about 0.000154%. This makes the combination a truly rare, and every poker player dreams of seeing it on his hand at least once in his life."
            )
        )

        comboData.add(
            Combination(
                "Street flash",
                R.drawable.combo_4,
                "Another very strong combination is the street flash. It combines two card values: straight (five consecutive cards) and flush (all cards of the same suit). For example, 5, 6, 7, 8 and 9 worms are the perfect street flash.\n" +
                        "The probability of such a combination falling out is about 0.00139%. This is already higher than the royal flush, but it is still extremely rare."
            )
        )

        comboData.add(
            Combination(
                "Kare",
                R.drawable.combo_5,
                "A strong combination consisting of four cards of one face value and one additional one. For example, four eights and one any other denomination is the perfect square. The probability of getting such a combination is about 0.0240%. The kare can become an excellent weapon for a poker player, giving high chances of winning."
            )
        )


        comboData.add(
            Combination(
                "Full House ",
                R.drawable.combo_fullhouse,
                "Combines three cards of the same denomination and a pair of cards of the other denomination. For example, three jacks and two sevens are a great full house. This combination is considered strong and often becomes a decisive factor in the game. The probability is about 0.1441%, which makes the full house quite rare, but still encountered in practice."
            )
        )


        comboData.add(
            Combination(
                "Flush",
                R.drawable.combo_6,
                "Five cards of the same suit, but not necessarily consecutive. For example, five heart cards - and voila, the flush is ready!\n" +
                        "The probability of getting such a combination is about 0.1965%. Relatively frequent, often becomes a strong base for a winning hand."
            )
        )


        binding.combinationsRecycle.layoutManager =
            LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)

        val adapter = CombinationsAdapter(comboData)
        binding.combinationsRecycle.adapter = adapter

        binding.backBtn.setOnClickListener {
            onBackPressed()
        }

        binding.nextBtn.setOnClickListener {
            binding.combinationsRecycle.postDelayed(Runnable {
                binding.combinationsRecycle.smoothScrollToPosition(adapter.pos + 1)
            }, 500)
            adapter.notifyDataSetChanged()
        }
    }

    private fun setFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}