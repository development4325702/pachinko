package com.pachinko.poker.star.rules.app.screens

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pachinko.poker.star.rules.app.R
import com.pachinko.poker.star.rules.app.adapters.RulesAdapter
import com.pachinko.poker.star.rules.app.databinding.ActivityRulesBinding
import com.pachinko.poker.star.rules.app.models.Rules

class RulesActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRulesBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRulesBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setFullScreen()

        binding.rulesRecycle.layoutManager =
            LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)

        val rulesData = ArrayList<Rules>()
        rulesData.add(
            Rules(
                "rules of combinations",
                "In various variants of poker, there are some variations of the rules for collecting combinations, which adds interest and diversity to the game. Here are some famous varieties of poker and their differences in the rules of collecting card combos:\n" +
                        "Texas Hold'em: one of the most popular and widespread varieties of poker.\u2028Each player is dealt two cards (closed), and five community cards (open) are revealed on the table. Players need to collect a combination of their two cards and any three of the common cards.\u2028Thus, possible combinations can consist of two player cards and three on the table, or even be formed of only five community cards on the table. All standard combinations remain in force.\n" +
                        "Omaha: another popular option that has similar rules to Texas Hold'em, but with one important difference: each player is dealt four hole cards instead of two.\u2028However, when forming a combination, two of the player's four cards and three of the five common cards are necessarily used. This change in rules leads to the fact that stronger combinations are often formed in Omaha, and the game becomes more dynamic.",
                null
            )
        )

        rulesData.add(
            Rules(
                "weak hands",
                "In poker, the terms \"strong\" and \"weak\" hands refer to combinations of cards that have a high or low value, respectively. Determining which hand is considered strong or weak depends on the specific type of poker and the current situation in the hand.\n" +
                        "Strong hands in poker are powerful combinations that have a high chance of winning. For example, the combinations of \"Royal Flash\", \"Street Flash\", \"Kare\", \"Full House\" and \"Flash\" are considered strong, as they are rarely formed and usually lead to a win in the hand.\n" +
                        "Strong hands are usually determined at the end of the hand, when all the cards are revealed and players have the opportunity to fully evaluate the combinations. Players can also count the hands of other players based on the bets that are made in the process.",
                null
            )
        )

        rulesData.add(
            Rules(
                "What is a draw hand",
                "Draw hands (or just \"draw\") in poker are combinations of cards that have not yet created a complete set, but have the potential to improve to a stronger hand on the next streets. Usually occur in the early stages of the hand, when the player has only part of the necessary cards to form a stronger combination.\n" +
                        "Examples of draw hands:\n" +
                        "Open street draw: the player has 7 and 8 in his hands, and on the table there are 5, 6, 9. In this case, the player collected an open straight draw, because only one of the two cards (6 or 10) is missing to collect a straight. The player can wait for one of these cards on the next streets and improve his hand.",
                R.drawable.rules_1
            )
        )

        rulesData.add(
            Rules(
                "Kicker in poker",
                "Kickers become important when players have the same card combinations in their hands or on the table, and it is not possible to determine the winner only by the main hand. In this case, the decisive factor will be the rank of the card, which is not included in the main combination - it is what is called a kicker.\n" +
                        "Examples of using kickers:\n" +
                        "The highest card: if the players have no combinations and the winner is determined by the highest card, this card becomes a kicker. For example, the player has 10 and 7 in his hands, and the opponent has 10 and 6. On the table there are cards 4, 8, Dame, Jack and King. Both players have not collected a combination, and the winner will be determined by the highest card - in this case, the King. The king becomes a kicker and brings victory.",
                null
            )
        )

        rulesData.add(
            Rules(
                "Dead hands",
                "In poker, the term \"dead hands\" refers to combinations of cards that cannot improve to strong combinations or lead to winning the hand. These hands are deprived of the prospect of winning and, as a rule, players should not invest additional bets in them.\n" +
                        "For example, if a player has a weak pair and there is already a stronger pair on the table, this pair becomes dead. Also, if all the cards that can improve the hand are already on the table or with other players, such opportunities become dead. Some combinations also have a very low chance of improvement, which makes them with dead hands.\n" +
                        "Identifying dead hands will help to avoid unnecessary bets on combinations with low prospects. Probabilities analysis and understanding the strength of the hand will help to make more deliberate decisions and increase the chances of success.",
                null
            )
        )

        rulesData.add(
            Rules(
                "best combination",
                "When controversial situations arise and several players have the same or highly comparable card combinations, the winner is determined based on the established rules for comparing cards.\n" +
                        "Basic rules for determining the winner in controversial situations:\n" +
                        "First, the main combinations of players are compared. The highest combination wins.\n" +
                        "If the main combinations are equal, additional cards (kickers) are compared. The winner is determined by the highest kicker.\n" +
                        "If players have equal combinations and kickers, the pot is divided equally between them.",
                android.R.drawable.screen_background_light_transparent
            )
        )

        rulesData.add(
            Rules(
                "Tips for beginners",
                "To succeed in poker, to collect strong combinations, you need to study the rules and regular practice. Tips for beginners to improve the game:\n" +
                        "Play carefully at the beginning. Start by playing strong hands and avoid hands with low cards.\n" +
                        "Position at the table. Consider the position and play more aggressively, speaking last.\n" +
                        "Learn to read rivals. Carefully monitor the bets and behavior of other players to identify their strategies and strong/weak hands.\n" +
                        "Ability to bluff. Bluffing is an important aspect of poker, but use it wisely and only when you have reason to believe that it will work.",
                R.drawable.rules_2
            )
        )


        val adapter = RulesAdapter(rulesData)
        binding.rulesRecycle.adapter = adapter

        binding.backBtn.setOnClickListener {
            onBackPressed()
        }

        binding.nextBtn.setOnClickListener {
            binding.rulesRecycle.postDelayed(Runnable {
                binding.rulesRecycle.smoothScrollToPosition(adapter.pos + 1)
            }, 500)
            adapter.notifyDataSetChanged()
        }
    }

    private fun setFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}