package com.pachinko.poker.star.rules.app.screens

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.pachinko.poker.star.rules.app.databinding.ActivityPachinkoSplashBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class PachinkoSplash : AppCompatActivity() {

    private lateinit var binding: ActivityPachinkoSplashBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPachinkoSplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setFullScreen()

        GlobalScope.launch {
            delay(2650)
            val intent = Intent(this@PachinkoSplash, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun setFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}